package edu.ncsu.lubick.localHub.videoPostProduction.animation;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

public interface KeyPressAnimation {

	public void drawAnimatedSegment(Graphics g, BufferedImage activatedImage);
}
